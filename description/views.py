from django.shortcuts import render
from django.http import HttpResponse
from .forms import DescForms
from .models import Description
from django.views.decorators.http import require_POST

# Create your views here.

def index(request):
    list_form = Description.objects.order_by('jumlah')
    form = DescForms()
    content = {'list_form': list_form , 'form' : form }
    return render(request,'descriptionc1.html', content )

def tambahForms(request):
	form = DescForms(request.POST)

	if form.is_valid():
		descBaru = Description(jumlah =request.POST['jumlah'],ukuran=request.POST['ukuran'],catatan=request.POST['catatan'])
		descBaru.save()

	return redirect('description:index')
