from django.test import TestCase,Client
from description.models import Description

# Create your tests here.
class ngetest(TestCase):
    def test_open_deskripsi_page(self):
            response = Client().get('/')
            self.assertEqual(response.status_code, 200)
    def test_pages_contains_word(self):
            response = Client().get('/')
            self.assertContains(response, 'DESKRIPSI PRODUK')
            self.assertEqual(response.status_code, 200)
    def test_pages_contains_word_Pembelian(self):
            response = Client().get('/')
            self.assertContains(response, 'PEMBELIAN')
            self.assertEqual(response.status_code, 200)
    def test_pages_contains_word_Ukuran(self):
            response = Client().get('/')
            self.assertContains(response, 'Ukuran')
            self.assertEqual(response.status_code, 200)
    def test_pages_contains_word_Jumlah(self):
            response = Client().get('/')
            self.assertContains(response, 'Jumlah')
            self.assertEqual(response.status_code, 200)





