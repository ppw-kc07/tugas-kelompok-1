Nama:  
Nevil Philia Pramuda 1806191553  
Titus DP  1806191755
Maulana Wildan 1806191641  
Ryan Karyadiputera Setia 1806205211


[![pipeline status](https://gitlab.com/ppw-kc07/tugas-kelompok-1/badges/master/pipeline.svg)](https://gitlab.com/ppw-kc07/tugas-kelompok-1/commits/master)

Link Heroku : https://wildanshoe.herokuapp.com/


Pada tugas kelompok 1 PPW kali ini, kami akan membuat market place tetapi khusus untuk sepatu. Website ini terdiri dari warna serta fitur-fitur yang cukup sederhana sehingga mudah untuk dipahami oleh orang awam. Nama market place kami adalah "WIL&”. Kami membuat form untuk pembeli atau calon pembeli memberikan tempat serta konfirmasi pembelian sepatu yang sudah dipilih. Tujuan dari website ini adalah menjadi media untuk mempermudah jual-beli sepatu.

Website kami terdiri dari 4 halaman, yaitu home, kategori yang dipillih, deskripsi barang, dan form pembelian. Kekurangan dari website kami yaitu tidak adanya fitur login user sehingga relationship dengan customers menjadi kurang terikat.

*  Home: 
Home adalah halaman yang menampilkan sepatu dari beberapa kategori dengan informasi harga.
*  Kategori:
Halaman  ini berisi kategori sepatu yang dijual di website kami, contoh: sepatu formal, sepatu olahraga, dan sepatu casual.
*  Deskripsi Barang:
Halaman deskripsi berisi informasi yang lebih mendetail tentang produk yang diinginkan.
*  Form pembelian:
Halaman ini berisi form yang harus diisi ketika calon pembeli ingin melakukan pembelian. Form tersebut meliputi nama, alamat, nomor hp, dan metode pembayaran.